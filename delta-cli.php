<?php

/* @var $project \DeltaCli\Project */
use DeltaCli\Config\Database\DatabaseFactory as Db;

$project->setName('drgoodbinder');

$project->applyTemplate(
    $project->wordPressTemplate()
);

$project->getEnvironment('vagrant')->getManualConfig()
    ->addDatabase(Db::createInstance('mysql', 'drgoodbinder', 'drgoodbinder', 'drgoodbinder', 'localhost'));
