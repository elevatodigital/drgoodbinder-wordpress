<?php get_header(); ?>
	<main class='main-page-wrapper default-page'>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			<?php get_template_part('partials/home-hero'); ?>
			<?php get_template_part('partials/home-specialties'); ?>
			<?php get_template_part('partials/home-intro-cta'); ?>
			<?php get_template_part('partials/home-featured-conditions'); ?>
			
			<div class="col-ctas flex">
				<?php get_template_part('partials/schedule-appt-cta'); ?>
				<?php get_template_part('partials/home-upcoming-event'); ?>
			</div>

		<?php endwhile; endif; ?>
		
		<?php get_template_part('partials/theme-slider'); ?>
		<?php get_template_part('partials/testimonials-slider'); ?>
		<?php get_template_part('partials/contact-cta'); ?>
		
	</main>
<?php get_footer(); ?>