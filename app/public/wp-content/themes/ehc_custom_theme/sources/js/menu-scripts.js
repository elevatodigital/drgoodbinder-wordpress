jQuery( document ).ready(function( $ ) {

	$("a.expanded-menu").on('click',function(e){
		e.preventDefault();
		$(this).toggleClass('active');
	});

    $("#mobile-active").on("click",function(e){
        e.preventDefault();

        if( !( $(this).hasClass('active') ) ){
            $(this).addClass('active');
            $("#mobile-active + .main-navigation").animate({
            	'min-height':"100vh",
            },600,function(){
            	$(this).addClass('active');
           		$(this).attr("style","");
            });

        }else{
            $(this).removeClass('active');
            $("#mobile-active + .main-navigation").animate({
            	'min-height' : '0',
            },600,function(){
            	$(this).removeClass('active');
            	$(this).attr('style', '');
            });
        }
    });

    $("#mobile-active-two").on("click",function(e){
        e.preventDefault();

        if( !( $(this).hasClass('active') ) ){
            $(this).addClass('active');
            $("#mobile-active-two + .main-navigation").animate({
            	'min-height':"100vh",
            },600,function(){
            	$(this).addClass('active');
           		$(this).attr("style","");
            });

        }else{
            $(this).removeClass('active');
            $("#mobile-active-two + .main-navigation").animate({
            	'min-height' : '0',
            },600,function(){
            	$(this).removeClass('active');
            	$(this).attr('style', '');
            });
        }
    });

    $("a").on('click',function(e){
    	if( $(this).attr('href') == "#" ){
    		e.preventDefault();
    	}
    });

    $(".main-navigation .menu-item > a ").on('click',function(e){

    	if( $(this).parents('.main-navigation').hasClass('active')  ){

    		/*We actually have children */
    		if( $(this).parent().hasClass('menu-item-has-children') ){

    			/* This is already open */
    			if( $(this).parent().hasClass('active') ){
    				$(".main-navigation .menu-item").removeClass("active");    				
					$(this).parent().removeClass('active');
					if( $(this).attr("href") == '#'){
						e.preventDefault();
					}

				/* Not open */
    			}else{
    				$(".main-navigation .menu-item").removeClass("active");
    				e.preventDefault();
    				$(this).parent().addClass('active');	
    			} 			
    		}    		
    	}
    });


    /*In Page Navigation */
    $('a.navigate').on('click',function(e){
    	e.preventDefault();
    	var theList = $(this).siblings('ul');

    	if( $(this).hasClass('active') ){
    		$(this).removeClass('active');
    		theList.slideUp(function(){
    			$(this).removeClass('active');
    			$(this).attr('style','');
    		});
    	}else{
    		$(this).addClass('active');
    		theList.slideDown(function(){
    			$(this).addClass('active');
    			$(this).attr('style','');
    		});
    	}    	
    });

    $(".top-navigation > .inset > ul > li > a").on('click',function(e){
    	$(this).parent().parent().removeClass('active');
    });
});