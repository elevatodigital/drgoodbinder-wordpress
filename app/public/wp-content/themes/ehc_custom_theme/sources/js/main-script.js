jQuery( document ).ready(function( $ ) {
	$("form.filter-form select ").on("change",function(){
		$(this).parents('form').submit();
	});
	$(".ginput_container > input").focus(function(){
		$(this).parent().parent().addClass('active');
	});
	$(".ginput_container > input").focusout(function(){
		if( !$(this).val() ){
			$(this).parent().parent().removeClass('active');
		}		
	});
	$(".ginput_container > textarea").focus(function(){
		$(this).parent().parent().addClass('active');
	});
	$(".ginput_container > textarea").focusout(function(){
		if( !$(this).val() ){
			$(this).parent().parent().removeClass('active');
		}		
	});
	
	let $slider = $('.testimonials-slider .slider-container');
	// console.log($slider);
	if ($slider.length) {
		$slider.slick({
			infinite:	      false,
			autoplay: 			true,
  		autoplaySpeed: 	5000,
			slidesToShow:   3,
			slidesToScroll: 1,
			centerMode: 		true,
			focusOnSelect: 	true,
			dots: 					false,
			responsive:     [
				{
					breakpoint: 768,
					settings:{
						slidesToShow: 1,
					}
				},
			],
		});
		
		let setSlidesActive = function(index) {
			let $currentSlide = $slider.find('.slide[data-slick-index="' + index + '"]'),
					$prevSlide    = $currentSlide.prev(),
					$nextSlide    = $currentSlide.next();
			
			$currentSlide.addClass('slick-active');
			$prevSlide.addClass('slick-active');
			$nextSlide.addClass('slick-active');
		};
		
		setSlidesActive(0);
		
		$slider.on('afterChange', function(e, slick, currentSlide) {
			setSlidesActive(currentSlide);
		});
	}
	
	if( $(".theme-slider-outer-wrapper").length ){
		$(".theme-slider-outer-wrapper > .theme-slider-slide-container").slick({
			slidesToShow: 4,
			infinite: true,
			arrows:false,
			speed:4000,
			autoplay:true,
			autoplaySpeed:1500,
			pauseOnFocus:false,
			pauseOnHover:false,
			responsive:[
			{
				breakpoint: 1200,
				settings:{
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 968,
				settings:{
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 680,
				settings:{
					slidesToShow: 1,
				}
			}
			]
		});
	}

	if( $(".featured-video-slider-outer-wrapper").length ){
		$(".featured-video-slider-outer-wrapper > .featured-videos-slider").slick({
			slidesToShow:3,
			variableWidth:true,
			arrows:false,
			autoplay:true,
			centerMode:true,
			dots:true,
			responsive:[
				{
					breakpoint: 750,
					settings:{
						centerMode:false,
						slidesToShow: 1,
						variableWidth:false,
					}
				},
			]
		});
	}
});
