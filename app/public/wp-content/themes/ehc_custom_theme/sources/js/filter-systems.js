jQuery( document ).ready(function( $ ) {

	$(".advanced-project-filter-form > .filter-toggle").on('click',function(e){
		e.preventDefault();

		if( $(this).parent().hasClass('active') ){

			$(this).siblings("form").slideUp(function(){

				$(this).parent().removeClass('active');

				$(this).attr("style",'');

			});

		}else{
			$(this).siblings("form").slideDown(function(){

				$(this).parent().addClass('active');

				$(this).attr("style",'');

			});
		}
	});
	$(".collective-section > .section-title").on('click',function(e){
		if( $(this).hasClass('active') ){
			$(this).removeClass('active');
			$(this).siblings(".checkbox-wrapper").slideUp(function(){
				$(this).removeClass('active');
				$(this).attr("style", '');
			});

		}else{
			$(this).addClass('active');
			$(this).siblings(".checkbox-wrapper").slideDown(function(){
				$(this).addClass('active');
				$(this).attr("style", '');
			});
		}
	});
	$(".sort-form-wrapper > form select").on("change",function(){
		$(this).parents('form').submit();
	});	
});