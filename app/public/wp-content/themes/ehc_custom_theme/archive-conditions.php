<?php get_header(); ?>
	<main class='main-page-wrapper default-page conditions-landing'>		
		<div class='page-header-section'>
			<div class='inset'>
				<h1><?php the_field('condition_landing_page_title','options'); ?></h1>
			</div>
		</div>
		<div class='page-content-wrapper'>
			<div class='conditions-landing'>
				<div class='full-width-subtext'><?php the_field('condition_landing_page_content_subhead','options'); ?></div>
				<?php if ( have_posts() ) :
					?><div class='conditions-card-wrapper'><div class='insert'><?php
						while ( have_posts() ) : the_post();
							get_template_part('partials/conditions-card');
						endwhile;
				?></div><?php VentaHelperClass::pagination(); ?></div><?php endif; ?>
			</div>
		</div>	
	</main>
<?php get_footer(); ?>
