<?php
get_header();
$featuredArray = get_field('featured_videos','options');
$allCategories = get_terms( array('taxonomy' => 'video-category','hide_empty' => true,) );
$currentCat = get_query_var('video-category',false);
?>
	<main class='main-page-wrapper default-page'>
		<?php if( $featuredArray ): ?>
			<?php $featuredVideos = new WP_Query(array(
				'post_type' => 'videos',
				'posts_per_page' => -1,
				'post__in' => $featuredArray,
			));
			if( $featuredVideos->have_posts() ): ?>
				<div class='featured-video-slider-outer-wrapper'>
					<div class='section-title'>
						<h2>Featured Videos</h2>
					</div>
					<div class='featured-videos-slider'><?php
						while( $featuredVideos->have_posts() ): $featuredVideos->the_post();
							?><article class="video-card-wrapper">
								<?php $terms = wp_get_post_terms( get_the_id(), 'video-category' ); ?>								
								<div class='video-wrapper'>
									<a href="<?php echo 'https://www.youtube.com/embed/'.VentaHelperClass::get_youtube_video_id_from_url(get_field('video_url')).'&rel=0&showinfo=0&autoplay=1'; ?>" data-lity class='video-thumbnail video' <?php VentaHelperClass::inline_background_attach('https://img.youtube.com/vi/'.VentaHelperClass::get_youtube_video_id_from_url(get_field('video_url')).'/hqdefault.jpg');?> ></a>
								</div>								
								<div class='video-details'>
									<div class='video-top-details'>
										<?php if( $terms ): ?>
											<div class='categories'>
												<p><?php 
													foreach ($terms as $term) {
														?><span><?php echo $term->name; ?><span>, </span></span><?php
													}
												?></p>
											</div>
										<?php endif; ?>
										<p class='video-extras'>
											<span><i class="fas fa-calendar-alt"></i><?php echo get_the_date( 'j M y' ); ?></span>
											<?php if( get_field('runtime') ): ?><span><i class="fas fa-clock"></i><?php the_field('runtime'); ?></span> <?php endif; ?>
										</p>
									</div>
									<p class='video-title'><?php the_title(); ?></p>
								</div>
							</article><?php
						endwhile; 
					?></div>
				</div>
			<?php endif; wp_reset_postdata(); ?>			
		<?php endif; ?>
		<?php if ( have_posts() ) : ?>
			<div class='video-list-out-wrapper'>
				<div class='section-title'>
					<h3>Video Center</h3>
					<?php if( $allCategories ): ?>
						<div class='filter-form-wrapper'>
							<form class='filter-form' action="<?php echo get_post_type_archive_link( 'videos' ); ?>">
								<select name='video-category'>
									<option value="" disabled <?php if( $currentCat === false ) echo "selected"; ?>>Filter Videos</option>
									<option value="">All</option><?php 
									foreach ($allCategories as $cat) {
										?><option value="<?php echo $cat->slug; ?>" <?php if( $currentCat == $cat->slug ) echo "selected"; ?>><?php echo $cat->name; ?></option><?php
									}
								?></select>
								<i class="fas fa-angle-down"></i>
							</form>
						</div>
					<?php endif; ?>					
				</div>
				<div class='inset'><?php
					while ( have_posts() ) : the_post(); 
						get_template_part('partials/video-card');
					endwhile;
				?></div>
				<?php VentaHelperClass::pagination(); ?>
			</div>			
		<?php endif; ?>
	</main>
<?php get_footer(); ?>