<?php get_header(); ?>
	<main class='main-page-wrapper default-page'>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class='page-header-section'>
				<div class='inset'>					
				</div>
			</div>
			<div class='single-event-page-header'>
				<?php get_template_part('partials/event-card'); ?>
			</div>
			<div class='page-content-wrapper'>
				<div class='the-content'>
					<?php if( get_field('sub_head') ):?><div class='subtext'><?php the_field('sub_head'); ?></div><?php endif; ?>
					<div class='wsywig-content'><?php the_content(); ?></div>
				</div>
				<?php get_template_part('partials/events-side-bar'); ?>
			</div>	
		<?php endwhile; endif; ?>
	</main>
<?php get_footer(); ?>