<?php
function venta_custom_post_types(){
	loopable_custom_post_maker("success-stories", "Success Story", "Success Stories", "dashicons-testimonial",array('title','editor'),true,true);
	loopable_custom_post_maker("videos", "Video", "Videos", "dashicons-format-video",array('title'),true,true);
	loopable_custom_post_maker("conditions", "Condition", "Conditions", "dashicons-nametag",array('title','editor','excerpt','thumbnail'),true,true);
	loopable_custom_post_maker("events", "Event", "Events", "dashicons-calendar-alt",array('title','editor','thumbnail'),true,true);

	register_taxonomy('video-category', 'videos',
		array(
			'hierarchical' => true,
			'labels' => array(
				'name'                       => _x( 'Category', 'taxonomy general name', 'textdomain' ),
				'singular_name'              => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
				'search_items'               => __( 'Search Categories', 'textdomain' ),
				'popular_items'              => __( 'Popular Categories', 'textdomain' ),
				'all_items'                  => __( 'All Categories', 'textdomain' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => __( 'Edit Category', 'textdomain' ),
				'update_item'                => __( 'Update Category', 'textdomain' ),
				'add_new_item'               => __( 'Add New Category', 'textdomain' ),
				'new_item_name'              => __( 'New Category Name', 'textdomain' ),
				'add_or_remove_items'        => __( 'Add or remove Categories', 'textdomain' ),
				'not_found'                  => __( 'No Categories found.', 'textdomain' ),
				'menu_name'                  => __( 'Categories', 'textdomain' ),
		),
			'rewrite' => array( 'slug' => 'video-category' ),
			'show_ui' => true,
			'show_admin_column' => true,
		)
	);
	register_taxonomy('success-category', 'success-stories',
		array(
			'hierarchical' => true,
			'labels' => array(
				'name'                       => _x( 'Category', 'taxonomy general name', 'textdomain' ),
				'singular_name'              => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
				'search_items'               => __( 'Search Categories', 'textdomain' ),
				'popular_items'              => __( 'Popular Categories', 'textdomain' ),
				'all_items'                  => __( 'All Categories', 'textdomain' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => __( 'Edit Category', 'textdomain' ),
				'update_item'                => __( 'Update Category', 'textdomain' ),
				'add_new_item'               => __( 'Add New Category', 'textdomain' ),
				'new_item_name'              => __( 'New Category Name', 'textdomain' ),
				'add_or_remove_items'        => __( 'Add or remove Categories', 'textdomain' ),
				'not_found'                  => __( 'No Categories found.', 'textdomain' ),
				'menu_name'                  => __( 'Categories', 'textdomain' ),
		),
			'rewrite' => array( 'slug' => 'success-category' ),
			'show_ui' => true,
			'show_admin_column' => true,
		)
	);
	
	// register_taxonomy_for_object_type( 'locations', 'projects');
	// register_taxonomy('skills', 'projects',
	// 	array(
	// 		'hierarchical' => true,
	// 		'labels' => array(
	// 			'name'                       => _x( 'Skills', 'taxonomy general name', 'textdomain' ),
	// 			'singular_name'              => _x( 'Skill', 'taxonomy singular name', 'textdomain' ),
	// 			'search_items'               => __( 'Search Skills', 'textdomain' ),
	// 			'popular_items'              => __( 'Popular Skills', 'textdomain' ),
	// 			'all_items'                  => __( 'All Skills', 'textdomain' ),
	// 			'parent_item'                => null,
	// 			'parent_item_colon'          => null,
	// 			'edit_item'                  => __( 'Edit Skill', 'textdomain' ),
	// 			'update_item'                => __( 'Update Skill', 'textdomain' ),
	// 			'add_new_item'               => __( 'Add New Skill', 'textdomain' ),
	// 			'new_item_name'              => __( 'New Skill Name', 'textdomain' ),
	// 			'add_or_remove_items'        => __( 'Add or remove Skills', 'textdomain' ),
	// 			'not_found'                  => __( 'No Skills found.', 'textdomain' ),
	// 			'menu_name'                  => __( 'Skills', 'textdomain' ),
	// 	),
	// 		'rewrite' => array( 'slug' => 'skills' ),
	// 		'show_ui' => true,
	// 		'show_admin_column' => true,
	// 	)
	// );

}
add_action('init','venta_custom_post_types');

function loopable_custom_post_maker($slug, $name, $multiName, $dashicon,$supports,$hasArchive,$public){
    register_post_type( $slug, array(
        'labels'              => array(
            'name'                => __( $name ),
            'singular_name'       => __( $name ),
            'add_new_item'        => __( 'Add '.$multiName.'' ),
            'edit_item'           => __( 'Edit '.$name.'' ),
            'new_item'            => __( 'New '.$name.'' ),
            'view_item'           => __( 'View '.$name.'' ),
            'search_items'        => __( 'Search '.$name."'s" ),
            'not_found'           => __( 'No '.$multiName." found" ),
            'not_found_in_trash'  => __( 'No '.$multiName.' found in Trash' ),
            'menu_name'           => __( $multiName ), 
        ),
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,
        'menu_icon'           => $dashicon,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => $public,
        'exclude_from_search' => false,
        'has_archive'         => $hasArchive,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => array( 'slug' => $slug, 'with_front' => false ),
        'capability_type'     => 'post',
        'supports'            => $supports,
    ));
}