<?php
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' => 'Theme Options',
		'menu_title' => 'Theme Options',
		'menu_slug' => 'theme-options-page',
		'capability' => 'edit_posts',
		'redirect'		=> true,
	));
	acf_add_options_sub_page(array(
		'page_title' => 'Website Options',
		'menu_title' => 'Website Options',
		'parent_slug' => 'theme-options-page',
	));
	acf_add_options_sub_page(array(
		'page_title' => 'Contact Information',
		'menu_title' => 'Contact Information',
		'parent_slug' => 'theme-options-page',
	));
	acf_add_options_sub_page(array(
		'page_title' => 'Social Media',
		'menu_title' => 'Social Media',
		'parent_slug' => 'theme-options-page',
	));
	acf_add_options_sub_page(array(
		'page_title' => 'Landing Page Options',
		'menu_title' => 'Landing Page Options',
		'parent_slug' => 'theme-options-page',
	));
	acf_add_options_sub_page(array(
		'page_title' => 'Author Information',
		'menu_title' => 'Author Information',
		'parent_slug' => 'theme-options-page',
	));
}