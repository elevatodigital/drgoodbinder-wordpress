<?php

class VentaHelperClass{

	static function get_header_type( $id ) {
		/* Need to decide what header to load here */
		if( $id ){
			if(is_singular('staff')){
				return "solid";
			}elseif( has_post_thumbnail($id) || is_front_page() ){
				return "opacity";
			}else{
				return "solid";
			}
		}else{
			return "solid";
		}		
	}

	static function inline_background_attach($path){
		$output = "style='background: url(" . $path . ");background-size:cover;background-position:center center;background-repeat:no-repeat;'";
		echo $output;
	}

	static function attach_featured_image($id,$size){

		if( has_post_thumbnail($id) ):
			$path =  get_the_post_thumbnail_url($id,$size);
			$output = "style='background: url(" . $path . ");background-size:cover;background-position:center center;background-repeat:no-repeat;'";
			echo $output;
		endif;
	}
	static function attach_featured_image_top($id,$size){

		if( has_post_thumbnail($id) ):
			$path =  get_the_post_thumbnail_url($id,$size);
			$output = "style='background: url(" . $path . ");background-size:cover;background-position:top center;background-repeat:no-repeat;'";
			echo $output;
		endif;
	}

	static function phone_only_numbers($data){
		echo preg_replace('/[^0-9]+/', '', $data);
	}
	static function get_youtube_video_id_from_url($data){
		parse_str( parse_url( $data, PHP_URL_QUERY ), $my_array_of_vars );
		return $my_array_of_vars['v'];
	}

	static function phone_output($phone){
		$output = "".substr($phone, 0, 3)."-".substr($phone, 3, 3)."-".substr($phone,6);
		echo $output;
	}

	static function make_widget($name,$id,$description){

		register_sidebar( array(
			'name' => $name,
			'id' => $id,
			'description' => $description,
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
	}

	static function pagination($query = null) {		

		if($query){
			$wp_query = $query;
		}else{
			global $wp_query;
		}

		if($wp_query->max_num_pages > 1){ ?>
			<div class='pagination-wrapper'>
			<div class='pagination'><?php
				$big = 999999999; // need an unlikely integer

				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages
				) ); ?>

			</div></div><?php
		}		
	}
	static function remove_comments_menu(){
	  remove_menu_page( 'edit-comments.php' );  
	}
	static function breadcrumbs($postID,$isCat = false ){

		if(wp_get_post_parent_id( $postID ) != 0){
			echo "<div class='breadcrumbs'><p>";				
			$parent = wp_get_post_parent_id( $postID );

			if( wp_get_post_parent_id( $parent ) != 0 ){
				$grandParent = wp_get_post_parent_id( $parent );

				if( wp_get_post_parent_id( $grandParent  ) != 0 ){
					$greatgrandParent = wp_get_post_parent_id( $grandParent );
					echo "<a href='".get_the_permalink($greatgrandParent)."'>".get_the_title($greatgrandParent)."</a>";
					echo '<i class="fas fa-angle-right"></i>';	
				}

				echo "<a href='".get_the_permalink($grandParent)."'>".get_the_title($grandParent)."</a>";
				echo '<i class="fas fa-angle-right"></i>';				
			}

			echo "<a href='".get_the_permalink($parent)."'>".get_the_title($parent)."</a>";
			echo '<i class="fas fa-angle-right"></i>';
			if($isCat){
				echo "<a href='".get_the_permalink($postID)."'>".get_the_title($postID)."</a>";
				echo '<i class="fas fa-angle-right"></i>';	
			}
			echo "</p></div>";
		}
	}
	static function share_buttons(){ ?>
		<div id='social-sharing-venta' class='social-sharing'>
			<span>Share</span>
			<div class='share-links'>
				<a title="Share on Facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>" target="_blank">
					<i class="fab fa-facebook-f"></i>
				</a>
				<a title="Share on Twitter" href="https://twitter.com/share?text=<?php echo urlencode(get_the_title()); ?>%20@<?php urlencode(blogInfo('name')); ?>" target="_blank" rel="nofollow">
					<i class="fab fa-twitter"></i>
				</a>
				<a title="Share on LinkedIn" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_the_permalink()); ?>&title=<?php echo urlencode(get_the_title()); ?>&summary=&source=" target="_blank">
					<i class="fab fa-linkedin-in"></i>
				</a>
				<a title="Email" href="mailto:?subject=<?php echo urlencode(get_the_title()); ?>&body=<?php urlencode(blogInfo('name')); ?>-<?php echo urlencode(get_the_permalink()); ?>">
					<i class="fas fa-envelope"></i>
				</a>
			</div>
		</div>
	<?php
	}
	static function async_scripts($url){

    	if ( strpos( $url, '#asyncload') === false ){

    		return $url;
    	
    	}else if ( is_admin() ){

    		return str_replace( '#asyncload', '', $url );

    	}else{

    		return str_replace( '#asyncload', '', $url )."' async='async";
    	}
    }	
}
class venta_custom_menu_walker extends Walker_Nav_Menu{
 
    function start_lvl( &$output, $depth = 0, $args = array() ) {
    	$indent = str_repeat("\t", $depth);
	    $output .= "<a href='#' class='go-back'>MENU</a><div class='sub-menu-wrap'><ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
    	$indent = str_repeat("\t", $depth);
	    $output .= "$indent</ul></div>\n";
    }
}