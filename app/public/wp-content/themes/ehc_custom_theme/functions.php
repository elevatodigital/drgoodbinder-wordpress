<?php

add_filter( 'gform_tabindex', 'gform_tabindexer', 10, 2 );
function gform_tabindexer( $tab_index, $form = false ) {
$starting_index = 1000; // if you need a higher tabindex, update this number
if( $form )
add_filter( 'gform_tabindex_' . $form['id'], 'gform_tabindexer' );
return GFCommon::$tab_index >= $starting_index ? GFCommon::$tab_index : $starting_index;
}
add_image_size( 'high_res', 2000, 2000, false);

add_action( 'admin_menu', ['VentaHelperClass','remove_comments_menu'] );

if ( ! function_exists('venta_theme_setup')){

	function venta_theme_setup(){

		add_theme_support( 'post-thumbnails' );

		register_nav_menus( 
			array(				
		        'main-menu'	=> __( 'Main Menu' ),
		        'top-menu' => __('Top Menu'),
		        'tri-link-menu'	=> __( 'Tri-link Menu (Footer)' ),
	    	)
	    );	
	}
	function ehc_main_menu(){ ?>
		<div class="primary-menu">
			<a id="mobile-active" href=""><span>Menu</span><div class='icon'><i class="fas fa-bars fa-fw"></i><i class="fas fa-times fa-fw"></i></div></a>
		<?php
		wp_nav_menu( 
			array(
			'container' => false, 
			'theme_location' => 'main-menu',
			'fallback_cb' => false,
			'walker' => new venta_custom_menu_walker,
			'menu_class' => 'main-navigation',
			'depth' => 2,
		) ); ?>		
		</div>
		<?php
	}
	function ehc_top_menu(){ ?>
		<div class="primary-menu">
			<a id="mobile-active-two" href=""><span>More</span><div class='icon'><i class="fas fa-angle-down"></i></div></a>
		<?php
		wp_nav_menu( 
			array(
			'container' => false, 
			'theme_location' => 'top-menu',
			'fallback_cb' => false,
			'walker' => new venta_custom_menu_walker,
			'menu_class' => 'main-navigation',
			'depth' => 2,
		) ); ?>		
		</div>
		<?php
	}

	function ehc_tri_menu(){
		wp_nav_menu( 
			array(
			'container' => false, 
			'theme_location' => 'tri-link-menu',
			'fallback_cb' => false,
			'walker' => new venta_custom_menu_walker,
			'menu_class' => 'main-navigation',
			'depth' => 1,
		) );
	}
}
add_action( 'after_setup_theme', 'venta_theme_setup' );

if ( ! function_exists('venta_theme_scripts')){	
	function venta_theme_scripts() {				
		wp_enqueue_style("google_fonts", "https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i");
		wp_enqueue_style("google_fonts_2", "https://fonts.googleapis.com/css?family=PT+Serif");
		wp_enqueue_script( 'slick_js', get_template_directory_uri() . '/includes/js/slick.min.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'lightbox_js', get_template_directory_uri() . '/includes/js/lightbox.min.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'lity_scripts', get_template_directory_uri() . '/includes/js/lity.min.js', array( 'jquery' ), '', true );		
		wp_enqueue_script( 'wistia_scripts_one_js',"https://fast.wistia.com/assets/external/E-v1.js",array('jquery'),'',true);
		wp_enqueue_script( 'wistia_scripts_two_js', "https://fast.wistia.com/labs/crop-fill/plugin.js",array('jquery'),'',true);
        wp_enqueue_script( 'waypoints_scripts', get_template_directory_uri() . '/includes/js/jquery.waypoints.min.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'inview_scripts', get_template_directory_uri() . '/includes/js/jquery.inview.min.js', array( 'jquery' ), '', true );
		wp_enqueue_style( 'venta_theme_style', get_template_directory_uri() . '/includes/css/main.css' );
		wp_enqueue_script( 'venta_theme_main_scripts', get_template_directory_uri() . '/includes/js/main.js', array( 'jquery','slick_js','lightbox_js','lity_scripts','wistia_scripts_one_js','wistia_scripts_two_js','waypoints_scripts', 'inview_scripts' ), '', true );
		wp_enqueue_style( 'theme_style', get_stylesheet_uri() );	
	}
	add_action( 'wp_enqueue_scripts', 'venta_theme_scripts' );
}

require_once( get_stylesheet_directory() . '/libs/custom_posts.php');
require_once( get_stylesheet_directory() . '/libs/venta-helper-class.php');

if( class_exists('acf') ){
	require_once( get_stylesheet_directory() . '/libs/custom-field-code.php');
	/* add_filter('acf/settings/show_admin', '__return_false');  */
}


if( class_exists('acf') && class_exists( 'GFCommon' ) ){
	require_once( get_stylesheet_directory() . '/includes/acf-gravityforms-add-on/acf-gravityforms-add-on.php' );
}

VentaHelperClass::make_widget('Footer Widget Center','footer-1',"Footer Widget");
VentaHelperClass::make_widget('Footer Widget Right','footer-2',"Footer Widget");
VentaHelperClass::make_widget('Default Sidebar','side-bar',"Sidebar for default pages, and blog posts.");
VentaHelperClass::make_widget('Events Sidebar','events-side-bar',"Sidebar for event pages.");

function special_social_media(){

	if( get_field('facebook_url','options') || get_field('twitter_url','options') || get_field('instagram_url','options') || get_field('youtube_url','options') ):
		?><div class='social-media-links'><ul><?php

			if( get_field('facebook_url','options') ): 
				?><li><a href="<?php echo get_field('facebook_url','options'); ?>" target="_blank"><i class="fab fa-facebook-square"></i></a></li><?php
			endif; 

			if( get_field('twitter_url','options') ): 
				?><li><a href="<?php echo get_field('twitter_url','options'); ?>" target="_blank"><i class="fab fa-twitter-square"></i></a></li><?php
			endif;

			if( get_field('instagram_url','options') ):
				?><li><a href="<?php echo get_field('instagram_url','options'); ?>" target="_blank"><i class="fab fa-instagram"></i></a></li><?php
			endif;

			if( get_field('youtube_url','options') ):
				?><li><a href="<?php echo get_field('youtube_url','options'); ?>" target="_blank"><i class="fab fa-youtube-square"></i></a></li><?php
			endif;

			?></ul></div><?php
	endif;
}

function goodBinder_query_vars( $vars ){
	$vars[] = 'video-category';
	$vars[] = 'success-category';
	return $vars;
}
add_filter( 'query_vars', 'goodBinder_query_vars' );

function goodBinder_blog_excerpt( $length ) {
	return 15;
}

function goodBinder_sudden_excerpt_end($more) {
   global $post;
   return '...';
}
add_filter('excerpt_more', 'goodBinder_sudden_excerpt_end');

function goodBinder_master_query( $query ){	
	if( !is_admin() && $query->is_main_query() && is_post_type_archive('videos') ){
		$query->set('posts_per_page', 12);
	}
	if( !is_admin() && $query->is_main_query() && is_post_type_archive('success-stories') ){
		$query->set('posts_per_page', 12);
	}
	if( !is_admin() && $query->is_main_query() && is_post_type_archive('conditions') ){
		$query->set('posts_per_page', 12);
	}
	if( !is_admin() && $query->is_main_query() && is_post_type_archive('events') ){
		$query->set('posts_per_page', 2);
		$query->set( 'orderby','meta_value' );
        $query->set( 'meta_key','event_date' );
        $query->set( 'order','ASC' );
	}
}
add_action( 'pre_get_posts', 'goodBinder_master_query' );


add_action( 'template_redirect', 'vm_goodbinder_gate_single_videos' );
function vm_goodbinder_gate_single_videos() {

	if ( is_singular('videos') ) {
		wp_redirect( get_post_type_archive_link('videos'), 301 );
		exit;
	}
}

add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    if (9 === (int) $form['id']) {
        return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'>
            SUBMIT
            <i class='fad fa-arrow-right'></i>
        </button>";
    }
    
    return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'>
				<i class='fad fa-arrow-right'></i>
			</button>";
}


/** Service page cards shortcode */

function goodbinder_service_cards( $atts )
{
	ob_start();
	get_template_part('partials/service-specialties');
	return ob_get_clean();
}
add_shortcode( 'specialties_cards', 'goodbinder_service_cards' );
