<?php

$allCategories = get_terms( array('taxonomy' => 'success-category','hide_empty' => true,) );
$currentCat = get_query_var('success-category',false);

	get_header();
?>
	<main class='main-page-wrapper default-page'>
		<div class='page-header-section'>
			<div class='inset'>
				<h1><?php the_field('success_stories_landing_page_title','options'); ?></h1>
			</div>
		</div>
		<?php if ( have_posts() ) : ?>
			<div class='video-list-out-wrapper'>
				<?php if( get_field('success_stories_landing_page_subtext','options') ):
					?><div class='success-landing-subtext'><?php the_field('success_stories_landing_page_subtext','options'); ?></div><?php
				endif; ?>
				<div class='success-stories-landing'>
					<?php if( $allCategories ): ?>
						<div class='filter-form-wrapper'>
							<form class='filter-form' action="<?php echo get_post_type_archive_link( 'success-stories' ); ?>">
								<select name='success-category'>
									<option value="" disabled <?php if( $currentCat === false ) echo "selected"; ?>>Filter Stories</option>
									<option value="">All</option><?php 
									foreach ($allCategories as $cat) {
										?><option value="<?php echo $cat->slug; ?>" <?php if( $currentCat == $cat->slug ) echo "selected"; ?>><?php echo $cat->name; ?></option><?php
									}
								?></select>
								<i class="fas fa-angle-down"></i>
							</form>
						</div>
					<?php endif; ?>					
				</div>
				<div class='inset'><?php
					while ( have_posts() ) : the_post(); 
						get_template_part('partials/success-card');
					endwhile;
				?></div>
				<?php VentaHelperClass::pagination(); ?>
			</div>			
		<?php endif; ?>
	</main>
<?php get_footer(); ?>
