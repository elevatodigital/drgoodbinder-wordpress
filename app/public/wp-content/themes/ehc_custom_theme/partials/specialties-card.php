<?php if ( ! empty ( $args ) ): ?>
<a class="specialty-card" href="<?php echo esc_attr( $args['data']['link'] ); ?>">
  <header>
    <?php if ( $args['data']['icon'] ): ?>
    <i class="fad fa-<?php echo esc_attr( $args['data']['icon'] ); ?>"></i>
    <?php endif; ?>
    <div>
      <h2 class="title"><?php echo esc_html( $args['data']['title'] ); ?></h2>
    </div>
  </header>
  <div>
    <?php echo $args['data']['body']; ?>
  </div>
</a>
<?php endif; ?>
