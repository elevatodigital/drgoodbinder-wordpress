<section class="home-intro-cta">
  <div class="inset">
    <header>
      <i class="fad fa-heartbeat"></i>
      <div>
        <h2>
          <em>N</em>aturally optimizing your bio-chemistry to reduce the risk of or reverse chronic disease.
        </h2>
      </div>
    </header>
    <p class="text-center">
      <a href="https://drgoodbinder.com/about/">
        Learn More
        <i class="blue-arrow"></i>
      </a>
    </p>
	</div>
</section>