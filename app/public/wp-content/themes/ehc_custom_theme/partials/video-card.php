<article class="video-card-wrapper">
	<?php $terms = wp_get_post_terms( get_the_id(), 'video-category' ); ?>
	<?php if( $terms ): ?>
		<div class='categories'>
			<p><?php 
				foreach ($terms as $term) {
					?><span><?php echo $term->name; ?><span>, </span></span><?php
				}
			?></p>
		</div>
	<?php endif; ?>
	<div class='video-wrapper'>
		<a href="<?php echo 'https://www.youtube.com/embed/'.VentaHelperClass::get_youtube_video_id_from_url(get_field('video_url')).'&rel=0&showinfo=0&autoplay=1'; ?>" data-lity class='video-thumbnail video' <?php VentaHelperClass::inline_background_attach('https://img.youtube.com/vi/'.VentaHelperClass::get_youtube_video_id_from_url(get_field('video_url')).'/hqdefault.jpg');?> ></a>
	</div>
	<div class='video-details'>
		<p class='video-extras'>
			<span><i class="fas fa-calendar-alt"></i><?php echo get_the_date( 'j M y' ); ?></span>
			<?php if( get_field('runtime') ): ?><span><i class="fas fa-clock"></i><?php the_field('runtime'); ?></span> <?php endif; ?>
		</p>
		<p class='video-title'><?php the_title(); ?></p>
	</div>
</article>