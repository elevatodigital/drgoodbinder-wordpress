<article class='event-card-wrapper'>
	<a href="<?php the_permalink(); ?>">
		<div class='date-ticker'><?php $timeHere = strtotime( get_field('event_date') ); ?>
			<p class='day'><?php echo date('d',$timeHere); ?></p>
			<p class='month'><?php echo date('M',$timeHere); ?></p>
		</div>
		<div class='inset'>
			<div class='image-side'>				
				<div class='image-wrapper'><?php
					if( has_post_thumbnail() ):
						?><div class='image' <?php VentaHelperClass::attach_featured_image(get_the_id(),'large'); ?> ></div><?php
					else:
						?><div class='image' <?php VentaHelperClass::inline_background_attach( get_template_directory_uri() ."/includes/img/event-fallback.jpg" ); ?> ></div><?php
					endif;
				?></div>
			</div>
			<div class='content-side'>
				<div class='inset'>
					<p class='sub-text'><span><?php the_field('topic'); ?></span></p>
					<h3><?php the_title(); ?></h3>
					<div class='content-down'>
						<div class='wsywig-content'><?php the_field('location'); ?></div>
					</div>
				</div>
			</div>
		</div>
	</a>
</article>