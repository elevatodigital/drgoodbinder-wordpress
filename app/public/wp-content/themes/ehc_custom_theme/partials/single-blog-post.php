<?php add_filter( 'excerpt_length', 'goodBinder_blog_excerpt', 999 );
?><article class='single-blog-post'>
	<a href="<?php the_permalink(); ?>">
		<div class='blog-post-meta-data'>
			<span><i class="fas fa-calendar-alt"></i><?php echo get_the_date( 'j M y' ); ?></span>
			<span><i class="fas fa-user"></i><?php the_field('author_name','options'); ?></span>
			<div class='author-image'><?php
				?><img src="<?php echo get_field('blog_default_image','options')['sizes']['thumbnail']; ?>" alt="<?php the_field('author_name','options'); ?>"/><?php
			?></div>
		</div>
		<?php $categories = get_the_terms( get_the_id(),'category'); ?>
		<?php if( $categories ): ?>
			<div class='blog-post-categories'><p><?php
				foreach ($categories as $cat) {
					?><span><?php echo $cat->name; ?><span>, </span></span><?php
				}				
			?></p></div>
		<?php endif; ?>		
		<div class='blog-post-data'>
			<h3><?php the_title(); ?></h3>
			<div class='wsywig-content'><?php the_excerpt(); ?></div>
		</div>
	</a>
</article><?php remove_filter( 'excerpt_length', 'goodBinder_blog_excerpt'); ?>
