<?php if( is_singular( 'post' ) ):
	$thisOne = get_the_id();
	$recentPosts = new WP_Query(array(
		'post_type' => 'post',
		'posts_per_page' => 2,
		'post__not_in' => array($thisOne),
	));
else:
	$recentPosts = new WP_Query(array(
		'post_type' => 'post',
		'posts_per_page' => 2,
	));
endif; ?>
<?php if( $recentPosts->have_posts() ): ?>
	<div class='from-the-blog'>
		<div class='section-title'>
			<h2>FROM THE BLOG</h2>
		</div>
		<div class='dual-post'>
			<div class='insert'><?php
				while( $recentPosts->have_posts() ): $recentPosts->the_post();
				get_template_part('partials/single-blog-post');
				endwhile; 
			?></div>
		</div>
		<div class='link-wrapper'>
			<a href="<?php echo get_the_permalink(get_option('page_for_posts')); ?>"><span>BROWSE BLOG</span><i class='blue-arrow'></i></a>
		</div>
	</div>
<?php endif; wp_reset_postdata(); ?>
