<?php
if ( ! empty( get_field('card') ) ) {
  $specialties = get_field('card');
}
?>
  
<?php if ( $specialties ): ?>
<section class="service-specialties">
  <div class="flex">
    <?php 
      foreach ($specialties as $specialty):
        if (! empty( $specialty['card_link'] ) ) {
          $link = $specialty['card_link'];
        }
        if (! empty( $specialty['card_external_link'] ) ) {
          $link = $specialty['card_external_link'];
        }
    ?>
      <?php
      get_template_part('partials/specialties-card', null, [
        'data' => [
          'icon'  => $specialty['card_icon'],
          'title' => $specialty['card_title'],
          'body'  => $specialty['card_body'],
          'link'  => $link
        ]
      ]);
      ?>
    <?php endforeach; ?>
	</div>
</section>
<?php endif; ?>
