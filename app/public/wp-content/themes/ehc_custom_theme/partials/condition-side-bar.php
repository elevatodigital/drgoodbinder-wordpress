<?php $thisCondition = get_the_id(); 
	$allConditions = new WP_Query(array('post_type'=>'conditions','posts_per_page'=>-1,));
if( $allConditions->have_posts() ): 
	?><aside class='site-sidebar'>	
		<div class='blog-sidebar'>
			<ul><?php
				while( $allConditions->have_posts() ): $allConditions->the_post();
					?><li <?php if( get_the_id() === $thisCondition  ) echo "class='active'"; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li><?php
				endwhile;					
			?></ul>
		</div>
	</aside><?php
endif; wp_reset_postdata(); ?>