<article class='condition-card'>
	<a href="<?php the_permalink(); ?>">
		<div class='card-title'>
			<i class="fad fa-dna"></i>
			<h3><?php the_title(); ?></h3>
		</div>
		<div class='card-content'><?php the_excerpt(); ?></div>
		<div class='card-arrow'>
			<i class='blue-arrow'></i>
		</div>									
	</a>
</article>