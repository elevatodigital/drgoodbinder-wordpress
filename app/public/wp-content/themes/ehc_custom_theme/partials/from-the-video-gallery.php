<?php if( get_field('related_videos') || is_front_page() ):
	$videosHere = new WP_Query(array(
		'post_type' => 'videos',
		'posts_per_page' => 3,
		'post__in' => get_field('related_videos'),
	));
	if( $videosHere->have_posts() ): ?>
		<div class='from-video-gallery'>
			<div class='section-title'>
				<h2>FROM THE VIDEO GALLERY</h2>
			</div>
			<div class='videos-wrapper'>
				<div class='insert'><?php
					while( $videosHere->have_posts() ): $videosHere->the_post();
						get_template_part('partials/video-card');
					endwhile;
				?></div>
				<div class='link-wrapper'>
					<a href="<?php echo get_post_type_archive_link( 'videos' ); ?>"><span>BROWSE VIDEO GALLERY</span><i class='blue-arrow'></i></a>
				</div>
			</div>
		</div>
	<?php endif; wp_reset_postdata(); ?>
<?php endif; ?>
		