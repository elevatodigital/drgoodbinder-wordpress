<section class='home-hero'>
  <div class='video-wrapper'><?php the_field('wistia_background_video_data'); ?></div>
  <div class='home-hero-content'>					
    <h1><?php the_field('home_hero_large_text'); ?></h1>
    <h3><?php the_field('home_hero_subtext'); ?></h3>
    <?php if( have_rows('home_duo_links') ):
      ?><div class='duo-links'><?php
        while( have_rows('home_duo_links') ): the_row();									
          ?><p>
            <a href="<?php the_sub_field('button_link'); ?>" class="btn btn-primary">
              <?php the_sub_field('button_text'); ?>
            </a>
          </p><?php
        endwhile;
      ?></div><?php
    endif; 
    ?>
  </div>
</section>