<?php
$images = get_field('home_&_doc_slides','options');
if( $images ): ?>
	<div class='theme-slider-outer-wrapper'>
		<div class='slider-title'>
			<h3><?php the_field('home_&_doc_slider_title','options'); ?></h3>
		</div>
		<div class='theme-slider-slide-container'><?php
			foreach ($images as $image) {
				?><div class='image-wrapper'>
					<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['description']; ?>">
				</div><?php
			}
		?></div>
	</div>
<?php endif; ?>