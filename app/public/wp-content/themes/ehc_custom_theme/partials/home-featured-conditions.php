<?php 
if( get_field('featured_conditions') ):
  $conditions = new WP_Query(array(
    'post_type'      => 'conditions',
    'post__in'       => get_field('featured_conditions'),
    'posts_per_page' => -1,
  ));
?>
  
<?php if( $conditions->have_posts() ): ?>
  <section class="home-featured-conditions">
    <div class='conditions-header-area'>
      <h3><?php the_field('home_conditions_header'); ?></h3>
    </div>
    <div class='insert'>
      
      <?php while( $conditions->have_posts() ): $conditions->the_post();
        get_template_part('partials/conditions-card');
      endwhile;
      ?>
    
      <div class='link-wrapper'>
        <a href="<?php echo get_post_type_archive_link( 'conditions' ); ?>"><span>BROWSE ALL</span><i class='blue-arrow'></i></a>
      </div>
  
    </div>
    
  </section>
  <?php endif; wp_reset_postdata(); ?>
<?php endif; ?>