<?php
if ( ! empty( get_field('card') ) ) {
  $specialties = get_field('card');
}
?>
  
<?php if ( $specialties ): ?>
<section class="home-specialties">
  <div class="flex">
    <?php foreach ($specialties as $specialty): ?>
      <?php
      get_template_part('partials/specialties-card', null, [
        'data' => [
          'icon'  => $specialty['card_icon'],
          'title' => $specialty['card_title'],
          'body'  => $specialty['card_body'],
          'link'  => $specialty['card_link']
        ]
      ]);
      ?>
    <?php endforeach; ?>
	</div>
</section>
<?php endif; ?>
