<?php 
$allEvents = new WP_Query(array(
  'post_type'      => 'events',
  'posts_per_page' => 1,
  'orderby'        => 'meta_value',
  'meta_key'       => 'event_date',
  'order'          => 'ASC',
));

if ( $allEvents->have_posts() ):
  while( $allEvents->have_posts() ): $allEvents->the_post();
?>
<section class='home-upcoming-event'>
  <?php get_template_part('partials/event-card'); ?>
</section>

<?php
  endwhile;
endif;

wp_reset_postdata();
?>