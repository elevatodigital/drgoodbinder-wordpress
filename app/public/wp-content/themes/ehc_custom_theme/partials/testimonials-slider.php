<?php
	$testimonials = get_posts([
		'post_type' => 'success-stories'
	]);
?>

<?php if ( $testimonials ): ?>
<section class="testimonials-slider">
	<div class="slider-container">
		
		<?php foreach ($testimonials as $testimonial): ?>
			<div class="slide">
				<div class='video-wrapper'>
					<a href="<?php esc_attr_e( get_permalink($testimonial->ID) ); ?>" class="video-thumbnail success"
						<?php
						$thisVideo = get_field('header_video', $testimonial->ID);	
						
						VentaHelperClass::inline_background_attach(
							'https://img.youtube.com/vi/' . 
							VentaHelperClass::get_youtube_video_id_from_url(
								get_field( 'video_url', $thisVideo )
							) . '/hqdefault.jpg'
						);
						?>>
					</a>
				</div>
			</div>
		<?php endforeach; ?>

	</div>
</section>
<?php endif; ?>