<div class='meet-the-doctor'>
	<div class='content-wrapper'>
		<div class='title-bar'>
			<div class='image-wrapper'>
				<img src="<?php echo get_field('meet_the_doctor_image','options')['sizes']['thumbnail']; ?>" alt="Dr. G"/>
			</div>
			<span><?php the_field('meet_the_doctor_title','options'); ?></span>
		</div>
		<div class='wsywig-content'><?php the_field('meet_the_doctor_content','options'); ?></div>
	</div>
	<div class='link-wrapper'>
		<a href="<?php the_field('meet_the_doctor_link','options'); ?>"><span>Meet the Doctor</span><i class='blue-arrow'></i></a>
	</div>
</div>