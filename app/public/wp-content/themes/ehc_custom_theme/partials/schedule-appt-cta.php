<section class="schedule-cta">
  <a href="/contact-us" title="Contact us" class="flex align-items-center justify-content-center">
    <i class="fad fa-calendar-check"></i>
    <div class="content">
      <h3>Schedule my appointment</h3>
      <p>
        Let’s explore how functional medicine can help you
      </p>
      <i class="blue-arrow"></i>
    </div>
  </a>
</section>