jQuery( document ).ready(function( $ ) {

	$(".advanced-project-filter-form > .filter-toggle").on('click',function(e){
		e.preventDefault();

		if( $(this).parent().hasClass('active') ){

			$(this).siblings("form").slideUp(function(){

				$(this).parent().removeClass('active');

				$(this).attr("style",'');

			});

		}else{
			$(this).siblings("form").slideDown(function(){

				$(this).parent().addClass('active');

				$(this).attr("style",'');

			});
		}
	});
	$(".collective-section > .section-title").on('click',function(e){
		if( $(this).hasClass('active') ){
			$(this).removeClass('active');
			$(this).siblings(".checkbox-wrapper").slideUp(function(){
				$(this).removeClass('active');
				$(this).attr("style", '');
			});

		}else{
			$(this).addClass('active');
			$(this).siblings(".checkbox-wrapper").slideDown(function(){
				$(this).addClass('active');
				$(this).attr("style", '');
			});
		}
	});
	$(".sort-form-wrapper > form select").on("change",function(){
		$(this).parents('form').submit();
	});	
});
jQuery( document ).ready(function( $ ) {
	$("form.filter-form select ").on("change",function(){
		$(this).parents('form').submit();
	});
	$(".ginput_container > input").focus(function(){
		$(this).parent().parent().addClass('active');
	});
	$(".ginput_container > input").focusout(function(){
		if( !$(this).val() ){
			$(this).parent().parent().removeClass('active');
		}		
	});
	$(".ginput_container > textarea").focus(function(){
		$(this).parent().parent().addClass('active');
	});
	$(".ginput_container > textarea").focusout(function(){
		if( !$(this).val() ){
			$(this).parent().parent().removeClass('active');
		}		
	});
	if( $(".theme-slider-outer-wrapper").length ){
		$(".theme-slider-outer-wrapper > .theme-slider-slide-container").slick({
			slidesToShow: 4,
			infinite: true,
			arrows:false,
			speed:4000,
			autoplay:true,
			autoplaySpeed:1500,
			pauseOnFocus:false,
			pauseOnHover:false,
			responsive:[
			{
				breakpoint: 1200,
				settings:{
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 968,
				settings:{
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 680,
				settings:{
					slidesToShow: 1,
				}
			}
			]
		});
	}

	if( $(".featured-video-slider-outer-wrapper").length ){
		$(".featured-video-slider-outer-wrapper > .featured-videos-slider").slick({
			slidesToShow:3,
			variableWidth:true,
			arrows:false,
			autoplay:true,
			centerMode:true,
			dots:true,
			responsive:[
				{
					breakpoint: 750,
					settings:{
						centerMode:false,
						slidesToShow: 1,
						variableWidth:false,
					}
				},
			]
		});
	}


	/* Sticky Sidebar */

	if( $("#website-sidebar").length ){
		var waypoint = new Waypoint({
	  		element: document.getElementById('website-sidebar'),
	  		handler: function(direction) { 			
	  			
	  			if( direction == 'down' ){

	  				$(this.element).addClass("sticky");

	  			}else{

	  				$(this.element).removeClass("sticky");
	  			}
	    		
	  		}
		});
	}	
});
jQuery( document ).ready(function( $ ) {

	$("a.expanded-menu").on('click',function(e){
		e.preventDefault();
		$(this).toggleClass('active');
	});

    $("#mobile-active").on("click",function(e){
        e.preventDefault();

        if( !( $(this).hasClass('active') ) ){
            $(this).addClass('active');
            $("#mobile-active + .main-navigation").animate({
            	'min-height':"100vh",
            },600,function(){
            	$(this).addClass('active');
           		$(this).attr("style","");
            });

        }else{
            $(this).removeClass('active');
            $("#mobile-active + .main-navigation").animate({
            	'min-height' : '0',
            },600,function(){
            	$(this).removeClass('active');
            	$(this).attr('style', '');
            });
        }
    });

    $("#mobile-active-two").on("click",function(e){
        e.preventDefault();

        if( !( $(this).hasClass('active') ) ){
            $(this).addClass('active');
            $("#mobile-active-two + .main-navigation").animate({
            	'min-height':"100vh",
            },600,function(){
            	$(this).addClass('active');
           		$(this).attr("style","");
            });

        }else{
            $(this).removeClass('active');
            $("#mobile-active-two + .main-navigation").animate({
            	'min-height' : '0',
            },600,function(){
            	$(this).removeClass('active');
            	$(this).attr('style', '');
            });
        }
    });

    $("a").on('click',function(e){
    	if( $(this).attr('href') == "#" ){
    		e.preventDefault();
    	}
    });

    $(".main-navigation .menu-item > a ").on('click',function(e){

    	if( $(this).parents('.main-navigation').hasClass('active')  ){

    		/*We actually have children */
    		if( $(this).parent().hasClass('menu-item-has-children') ){

    			/* This is already open */
    			if( $(this).parent().hasClass('active') ){
    				$(".main-navigation .menu-item").removeClass("active");    				
					$(this).parent().removeClass('active');
					if( $(this).attr("href") == '#'){
						e.preventDefault();
					}

				/* Not open */
    			}else{
    				$(".main-navigation .menu-item").removeClass("active");
    				e.preventDefault();
    				$(this).parent().addClass('active');	
    			} 			
    		}    		
    	}
    });


    /*In Page Navigation */
    $('a.navigate').on('click',function(e){
    	e.preventDefault();
    	var theList = $(this).siblings('ul');

    	if( $(this).hasClass('active') ){
    		$(this).removeClass('active');
    		theList.slideUp(function(){
    			$(this).removeClass('active');
    			$(this).attr('style','');
    		});
    	}else{
    		$(this).addClass('active');
    		theList.slideDown(function(){
    			$(this).addClass('active');
    			$(this).attr('style','');
    		});
    	}    	
    });

    $(".top-navigation > .inset > ul > li > a").on('click',function(e){
    	$(this).parent().parent().removeClass('active');
    });
});