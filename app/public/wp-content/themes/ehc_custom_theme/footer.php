
		<footer class='site-footer'>
			
			<div class='footer-top'>
				<div class='footer-logo'>
					<a href="<?php blogInfo('url'); ?>">
						<img src='<?php echo get_field('website_logo','options')['sizes']['large']; ?>' alt="<?php blogInfo('name'); ?>">
					</a>
				</div>
				<div class="newsletter">
					<h3>Sign up for our newsletter</h3>
					<?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]'); ?>
				</div>
			</div>
						
			<div class='footer-inset'>
				<div class='insert'><div class='footer-section-wrapper'>
						<aside class="custom-contact widget">
							<h3 class='widget-title'>Contact</h3>
							<div class='ehc-custom-widget'>
								<?php special_social_media(); ?>
								<p><a href="tel:<?php VentaHelperClass::phone_only_numbers( get_field('website_phone_number','options') ); ?>"><?php the_field('website_phone_number','options'); ?></a></p>
								<p><a href="mailto:<?php echo antispambot(get_field('main_website_contact_email','option')); ?>"><?php echo antispambot(get_field('main_website_contact_email','option')); ?></a></p>
								<p><?php the_field('website_physical_address','options'); ?></p>
							</div>
						</aside>
					</div><div class='footer-section-wrapper'>
						<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>					
							<?php dynamic_sidebar( 'footer-1' ); ?>						
						<?php endif; ?>
					</div><div class='footer-section-wrapper'>
						<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>					
							<?php dynamic_sidebar( 'footer-2' ); ?>						
						<?php endif; ?>
					</div></div>
			</div>
			
			<div class='footer-menu'>
				<div class='inset'><?php ehc_tri_menu(); ?></div>
			</div>
			
			<div class='footer-copyright'>
				<div class='inset'>
					<p class='copyright'>&copy; <?php echo date('Y'); ?> <?php blogInfo('name'); ?>. All Rights Reserved. Website by <a href="https://www.ventamarketing.com/" target="_blank">Venta Marketing</a></p>
				</div>
			</div>
				
		</footer>
		
		<div><?php wp_footer(); ?></div>		
		
	</body>
</html>