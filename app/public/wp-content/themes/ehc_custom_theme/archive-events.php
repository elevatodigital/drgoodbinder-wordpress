<?php get_header(); ?>
	<main class='main-page-wrapper default-page conditions-landing'>		
		<div class='page-header-section'>
			<div class='inset'>
				<h1><?php the_field('events_landing_page_title','options'); ?></h1>
			</div>
		</div>
		<div class='page-content-wrapper'>
			<div class='conditions-landing'>
				<div class='full-width-subtext'><?php the_field('events_landing_page_subtext','options'); ?></div>				
				<div class='events-landing-page'>
					<?php if ( have_posts() ) : while( have_posts() ): the_post(); ?>
						<?php get_template_part('partials/event-card'); ?>
					<?php endwhile; endif; ?>
				</div>
			</div>			
		</div>
		<?php if( get_field('events_landing_page_form','options') ): ?>
			<div class='events-landing-page-form-wrapper'>
				<div class='inset'>
					<?php gravity_form( get_field('events_landing_page_form','options'), true, true ); ?>
				</div>
				<div class="blue-bar"></div>
			</div>
		<?php endif; ?>	
	</main>
<?php get_footer(); ?>
