<?php get_header(); ?>
	<main class='main-page-wrapper default-page'>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php if( has_post_thumbnail() ): ?>
				<div class='page-header-section center w-image'>
					<div class='inset'>
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
				<div class='featured-image-wrapper-container'>
					<div class='featured-image-wrapper'>
						<div class='featured-image' <?php VentaHelperClass::attach_featured_image(get_the_id(),'full'); ?>></div>
					</div>
				</div>
			<?php else: ?>
					<div class='page-header-section'>
						<div class='inset'>
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
			<?php endif; ?>
			<div class='page-content-wrapper'>
				<div class='the-content'>
					<?php if( get_field('sub_head') ):?><div class='subtext'><?php the_field('sub_head'); ?></div><?php endif; ?>
					<div class='wsywig-content'><?php the_content(); ?></div>
					<?php get_template_part('partials/contact-cta'); ?>
				</div>
				<?php get_template_part('partials/side-bar'); ?>
			</div>
			<?php if( get_field('related_videos') ):?>

			<?php endif; ?>
			<?php get_template_part('partials/from-the-video-gallery'); ?>
			<?php get_template_part('partials/from-blog'); ?>			
		<?php endwhile; endif; ?>
	</main>
<?php get_footer(); ?>
