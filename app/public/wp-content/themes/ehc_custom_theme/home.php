<?php
$allCategories = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => true,
));
get_header(); ?>
	<main class='main-page-wrapper default-page'>				
		<div class='page-header-section'>
			<div class='inset'>
				<h1><?php echo get_the_title(get_option('page_for_posts'));?></h1>
			</div>
		</div>
		<div class='blog-page-menu-system'>
			<ul><?php
				foreach ($allCategories as $cat) {
					?><li><a href=""><?php echo $cat->name; ?></a></li><?php
				}						
			?></ul>
		</div>
		<div class='page-content-wrapper'>
			<div class='the-content'><?php
				if ( have_posts() ) :
					?><div class='blog-reel'><?php
					while ( have_posts() ) : the_post();
						get_template_part('partials/single-blog-post');
					endwhile;
					?></div><?php
					VentaHelperClass::pagination();
				endif; 
			?></div>
			<aside class='site-sidebar'>	
				<div class='blog-sidebar'>
					<ul><?php
						foreach ($allCategories as $cat) {
							?><li><a href="<?php echo get_term_link($cat); ?>"><?php echo $cat->name; ?></a></li><?php
						}						
					?></ul>
				</div>
			</aside>
		</div>	
	</main>
<?php get_footer(); ?>
