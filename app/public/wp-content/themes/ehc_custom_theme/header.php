<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title><?php echo bloginfo('name'); wp_title(); ?></title>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<header class='site-header'>
			<div class='upper-header'><?php
				special_social_media();
				ehc_top_menu(); 
			?></div>
			<div class='lower-header'><a id='site-root' href="<?php blogInfo('url'); ?>">
				<img src='<?php echo get_field('website_logo','options')['sizes']['large']; ?>' alt="<?php blogInfo('name'); ?>">
				</a><?php
				ehc_main_menu(); 
			?></div>	
		</header>